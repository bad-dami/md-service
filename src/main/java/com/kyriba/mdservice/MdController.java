/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice;

import com.kyriba.mdservice.currencycontrollers.CurrencyController;
import com.kyriba.mdservice.exchangeratecontrollers.ExchangeRateController;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;


/**
 * @author badr.dami
 * @since 19.2
 */
@Service
@RequiredArgsConstructor
public final class MdController
{

  final CurrencyController ccyController;
  final ExchangeRateController rateController;


  @Bean
  public RouterFunction<ServerResponse> routes()
  {
    return route()
        .path("/ccy", this::currencyRoutes)
        .path("/rate", this::exchangeRateRoutes)
        .build();
  }


  private RouterFunction<ServerResponse> currencyRoutes()
  {
    return route()
        .GET("/", ccyController::getAllCurrencies)
        .GET("/{id}", ccyController::getCurrencyById)
        .DELETE("/{id}", ccyController::deleteById)
        .POST("/", ccyController::saveCurrency)
        .build();
  }


  private RouterFunction<ServerResponse> exchangeRateRoutes()
  {
    return route()
        .GET("/stream", rateController::getRateStream)
        .build();
  }

}
