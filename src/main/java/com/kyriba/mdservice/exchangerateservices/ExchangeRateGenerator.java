/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.exchangerateservices;

import com.kyriba.mdservice.model.ExchangeRate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.util.stream.Stream;


/**
 * @author badr.dami
 * @since 19.2
 */
@Service
public final class ExchangeRateGenerator
{

  public Flux<ExchangeRate> generateExchangeRate(final String srcCcyName, final String dstCcyName)
  {
    return Flux.fromStream(Stream.generate(() -> {
      final BigDecimal rate = BigDecimal.valueOf(Math.random());
      return ExchangeRate.of(srcCcyName, dstCcyName, rate);
    }));
  }
}
