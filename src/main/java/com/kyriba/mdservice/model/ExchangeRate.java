/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.model;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;


/**
 * @author badr.dami
 * @since 19.2
 */
@Value
@Builder
@Document
public final class ExchangeRate
{
  @Id
  final String id;
  final String srcCcyId;
  final String dstCcyId;
  final BigDecimal rate;
  final Instant time;


  public static ExchangeRate of(final String srcCcyId, final String dstCcyId)
  {
    return builder()
        .id(UUID.randomUUID().toString())
        .srcCcyId(srcCcyId)
        .dstCcyId(dstCcyId)
        .rate(BigDecimal.valueOf(Math.random()))
        .time(Instant.now())
        .build();
  }


  public static ExchangeRate of(final String srcCcyId, final String dstCcyId, final BigDecimal rate)
  {
    return builder()
        .id(UUID.randomUUID().toString())
        .srcCcyId(srcCcyId)
        .dstCcyId(dstCcyId)
        .rate(rate)
        .time(Instant.now())
        .build();
  }


  @Override
  public String toString()
  {
    final String srcCcy = srcCcyId.split("-")[0];
    final String dstCcy = dstCcyId.split("-")[0];

    return "ExchangeRate{" +
        srcCcy + " -> " + dstCcy +
        ", rate=" + rate +
        ", time=" + time +
        '}';
  }
}
