/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.model;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


/**
 * @author badr.dami
 * @since 19.2
 */
@Value
@Document
public final class Currency
{
  @Id
  final String id;
  final String name;


  public static Currency of(final String name)
  {
    final String id = name + "-" + UUID.randomUUID().toString();
    return new Currency(id, name);
  }


  @Override
  public String toString()
  {
    return "Currency{" +
        ", name='" + name + '\'' +
        '}';
  }
}
