/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice;

import com.kyriba.mdservice.model.Currency;
import com.kyriba.mdservice.model.ExchangeRate;
import com.kyriba.mdservice.repositories.CurrencyRepository;
import com.kyriba.mdservice.repositories.ExchangeRateRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


/**
 * @author badr.dami
 * @since 19.2
 */
@Log4j2
@Component
@AllArgsConstructor
class DataInitializer
{

  final CurrencyRepository ccyRepository;
  final ExchangeRateRepository rateRepository;


  @EventListener(ApplicationReadyEvent.class)
  public void initialize()
  {
    final Flux<ExchangeRate> storeRates = ccyRepository.deleteAll()
        .thenEmpty(rateRepository.deleteAll())
        .thenMany(Flux.just("USD", "EUR", "GBP"))
        .map(Currency::of)
        .flatMap(ccyRepository::save)
        .flatMap(this::generateRates)
        .flatMap(rateRepository::save);

    final Mono<Long> rateReader = rateRepository
        .findAll()
        .count();

    storeRates
        .thenMany(rateReader)
        .subscribe(count -> log.info("ExchangeRate count = " + count));


  }


  private Flux<ExchangeRate> generateRates(final Currency srcCcy)
  {
    return ccyRepository.findByIdNotIn(List.of(srcCcy.getId()))
        .map(dstCcy -> ExchangeRate.of(srcCcy.getId(), dstCcy.getId()));
  }
}
