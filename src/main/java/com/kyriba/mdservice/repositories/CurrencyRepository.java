/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.repositories;

import com.kyriba.mdservice.model.Currency;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;


/**
 * @author badr.dami
 * @since 19.2
 */
@Repository
public interface CurrencyRepository extends ReactiveCrudRepository<Currency, String>
{
  Flux<Currency> findByIdNotIn(final List<String> ids);
}
