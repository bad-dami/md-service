package com.kyriba.mdservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MdServiceApplication
{

  public static void main(String[] args)
  {
    SpringApplication.run(MdServiceApplication.class, args);
  }

}