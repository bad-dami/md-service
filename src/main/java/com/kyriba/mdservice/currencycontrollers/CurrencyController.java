/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.currencycontrollers;

import com.kyriba.mdservice.model.Currency;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Function;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;


/**
 * @author badr.dami
 * @since 19.2
 */
public interface CurrencyController
{
  Mono<ServerResponse> getAllCurrencies(ServerRequest request);

  Mono<ServerResponse> saveCurrency(ServerRequest request);

  Mono<ServerResponse> deleteById(ServerRequest request);

  Mono<ServerResponse> getCurrencyById(ServerRequest request);

  default Function<Currency, Currency> validateCurrency()
  {
    return ccy -> Optional.of(ccy)
        .filter(c -> !StringUtils.isEmpty(c.getName()))
        .orElseThrow(() -> new ResponseStatusException(UNPROCESSABLE_ENTITY, "EEK! Empty Ccy Name"));
  }
}
