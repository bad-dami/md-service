/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.currencycontrollers;

import com.kyriba.mdservice.model.Currency;
import com.kyriba.mdservice.repositories.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Optional;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;


/**
 * @author badr.dami
 * @since 19.2
 */
@Service
@RequiredArgsConstructor
public final class CurrencyControllerImpl implements CurrencyController
{
  final CurrencyRepository ccyRepository;


  @Override
  public Mono<ServerResponse> getAllCurrencies(final ServerRequest request)
  {
    final Flux<Currency> currencies = ccyRepository
        .findAll()
        .delayElements(Duration.ofSeconds(1L));

    return ok()
        .contentType(MediaType.TEXT_EVENT_STREAM)
        .body(currencies, Currency.class);
  }


  @Override
  public Mono<ServerResponse> saveCurrency(final ServerRequest request)
  {
    Mono<Currency> savedCcy = request
        .bodyToMono(Currency.class)
        .map(validateCurrency())
        .flatMap(ccyRepository::save);

    return ok()
        .body(savedCcy, Currency.class);
  }


  @Override
  public Mono<ServerResponse> deleteById(final ServerRequest request)
  {
    final String ccyId = Optional.ofNullable(request.pathVariable("id"))
        .filter(id -> !id.isBlank())
        .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "misshapen id"));

    return ccyRepository.deleteById(ccyId)
        .flatMap(done -> ok().build());
  }


  @Override
  public Mono<ServerResponse> getCurrencyById(final ServerRequest request)
  {
    final String ccyId = Optional.ofNullable(request.pathVariable("id"))
        .filter(id -> !id.isBlank())
        .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "misshapen id"));

    return ok()
        .body(ccyRepository.findById(ccyId), Currency.class);
  }


}
