/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.exchangeratecontrollers;

import com.kyriba.mdservice.exchangerateservices.ExchangeRateGenerator;
import com.kyriba.mdservice.model.ExchangeRate;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import java.time.Duration;


/**
 * @author badr.dami
 * @since 19.2
 */
@Controller
@RequiredArgsConstructor
public final class ExchangeRateRSocketController
{
  private final ExchangeRateGenerator rateGenerator;


  @SuppressWarnings("unused")
  @MessageMapping("rateStream")
  Flux<ExchangeRate> rSocketExchangeRate(final RateRSocketRequest rateRequest)
  {
    final String srcCcyName = rateRequest.getSrcCcyName();
    final String dstCcyName = rateRequest.getDstCcyName();

    return rateGenerator.generateExchangeRate(srcCcyName, dstCcyName)
        .delayElements(Duration.ofSeconds(1L));
  }

}




