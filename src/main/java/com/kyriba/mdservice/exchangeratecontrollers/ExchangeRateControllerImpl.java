/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.exchangeratecontrollers;

import com.kyriba.mdservice.exchangerateservices.ExchangeRateGenerator;
import com.kyriba.mdservice.model.ExchangeRate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


/**
 * @author badr.dami
 * @since 19.2
 */
@Service
@RequiredArgsConstructor
public final class ExchangeRateControllerImpl implements ExchangeRateController
{
  private final ExchangeRateGenerator rateGenerator;


  @Override
  public Mono<ServerResponse> getRateStream(final ServerRequest request)
  {
    final String srcCcyId = request.queryParam("srcCcyId")
        .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "empty srcCcyId"));
    final String dstCcyId = request.queryParam("dstCcyId")
        .orElseThrow(() -> new ResponseStatusException(BAD_REQUEST, "empty dstCcyId"));

    final Flux<ExchangeRate> rates = rateGenerator.generateExchangeRate(srcCcyId, dstCcyId)
        .delayElements(Duration.ofSeconds(1L));
    //.mergeWith(simulateError());

    return ServerResponse
        .ok()
        .contentType(MediaType.TEXT_EVENT_STREAM)
        .body(rates, ExchangeRate.class);
  }


  private Flux<ExchangeRate> simulateError()
  {
    return Flux.error(() -> new ResponseStatusException(INTERNAL_SERVER_ERROR, "EEK! internal error"));
  }

}