/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.mdservice.exchangeratecontrollers;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


/**
 * @author badr.dami
 * @since 19.2
 */
public interface ExchangeRateController
{
  Mono<ServerResponse> getRateStream(ServerRequest request);
}
