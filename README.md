# md-service
Reactive demo md service

Currencies:

http -S :8081/ccy
http GET :8081/ccy/{id}
http DELETE :8081/ccy/{id}
http POST :8081/ccy/ < currency.txt

Rates:
http -S :8081/rate/stream srcCcyId=='USD' dstCcyId=='EUR'
